package br.com.softsite.pstmtnullsafe.resource;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SingletonPool<K, T> implements Pool<K, T> {

	private final HashMap<K, T> values = new HashMap<>();
	private final HashSet<K> keys = new HashSet<>();
	private final Function<K, T> resourceGetter;

	public SingletonPool(Function<K, T> resourceGetter) {
		this.resourceGetter = resourceGetter;
	}

	public void putResource(K key, T resource) {
		values.put(key, resource);
		keys.add(key);
	}

	@Override
	public T getResource(K key) {
		keys.add(key);
		return values.computeIfAbsent(key, resourceGetter);
	}

	@Override
	public List<T> getAllResources() {
		return values.values().stream().collect(Collectors.toList());
	}
	
	@Override
	public List<T> getAllocatedResources() {
		return values.entrySet().stream()
				.filter(es -> keys.contains(es.getKey()))
				.map(Entry::getValue)
				.collect(Collectors.toList());
	}

	@Override
	public void releaseAll() {
		keys.clear();
	}
}
