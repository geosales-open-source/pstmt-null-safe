package br.com.softsite.pstmtnullsafe.resource;

import java.util.List;

public interface Pool<K, T> {

	T getResource(K key);
	List<T> getAllocatedResources();
	List<T> getAllResources();
	void releaseAll();
}
