package br.com.softsite.pstmtnullsafe.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ReuseLastPool<K, T> implements Pool<K, T> {

	private final Function<K, T> resourceGetter;
	private final HashMap<K, Agrupamento<T>> agrupamentos = new HashMap<>();
	private K lastKey;
	private final ArrayList<T> reservedResources = new ArrayList<>();
	private T lastResource;

	public ReuseLastPool(Function<K, T> resourceGetter) {
		this.resourceGetter = resourceGetter;
	}

	@Override
	public T getResource(K key) {
		if (lastKey != null && key.equals(lastKey)) {
			return lastResource;
		}
		lastKey = key;
		Agrupamento<T> agrupamento = agrupamentos.computeIfAbsent(key, __ -> new Agrupamento<>());
		lastResource = agrupamento.getElemento(() -> resourceGetter.apply(key));
		reservedResources.add(lastResource);
		return lastResource;
	}

	@Override
	public List<T> getAllResources() {
		return agrupamentos.values().stream()
				.flatMap(agrup -> agrup.elementos.stream())
				.collect(Collectors.toList());
	}

	@Override
	public List<T> getAllocatedResources() {
		return reservedResources;
	}

	@Override
	public void releaseAll() {
		lastKey = null;
		lastResource = null;
		reservedResources.clear();
		agrupamentos.values()
			.stream()
			.forEach(Agrupamento::reset);
	}

	private static class Agrupamento<T> {
		private int livres;
		private final ArrayList<T> elementos = new ArrayList<>();

		void add(T e) {
			elementos.add(e);
		}

		void reset() {
			livres = elementos.size();
		}

		T getElemento(Supplier<T> lazyGenerator) {
			if (livres != 0) {
				int p = elementos.size() - livres;
				livres--;
				return elementos.get(p);
			}
			T e = lazyGenerator.get();
			add(e);
			return e;
		}
	}
}
