package br.com.softsite.pstmtnullsafe.function;

import java.sql.SQLException;
import java.util.function.Consumer;

import br.com.softsite.pstmtnullsafe.SQLRuntimeException;

public interface SQLConsumer<T> {
	void accept(T param) throws SQLException;

	default Consumer<T> toConsumer() {
		return (t) -> {
			try {
				this.accept(t);
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		};
	}
}