package br.com.softsite.pstmtnullsafe.function;

import java.sql.SQLException;

public interface SQLTriConsumer<T, U, V> {
	void accept(T param1, U param2, V param3) throws SQLException;
}