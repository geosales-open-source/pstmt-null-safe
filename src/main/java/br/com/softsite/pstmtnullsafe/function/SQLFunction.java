package br.com.softsite.pstmtnullsafe.function;

import java.sql.SQLException;
import java.util.function.Function;

import br.com.softsite.pstmtnullsafe.SQLRuntimeException;

public interface SQLFunction<I,O> {
	O apply(I param) throws SQLException;

	default Function<I,O> toFunction() {
		return (t) -> {
			try {
				return this.apply(t);
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		};
	}

	public static <I,O> Function<I,O> staticToFunction(SQLFunction<I,O> f) {
		return f.toFunction();
	}
}