package br.com.softsite.pstmtnullsafe.jdbc;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

class PstmtImplNoNulls implements PreparedStatement {

	final int[] mapaIdxJdbc2IdxTrabalho;
	final PreparedStatement pstmtInternaReal;

	PstmtImplNoNulls(NuloIds nulos, int questCount, PreparedStatement pstmtInternaReal) {
		mapaIdxJdbc2IdxTrabalho = new int[questCount + 1];
		if (questCount > 0) {
			mapaIdxJdbc2IdxTrabalho[1] = 1;
			for (int i = 2; i <= questCount; i++) {
				mapaIdxJdbc2IdxTrabalho[i] = mapaIdxJdbc2IdxTrabalho[i - 1] + (nulos.isNull(i - 1) ? 0 : 1);
			}
		}
		this.pstmtInternaReal = pstmtInternaReal;
	}

	private int fromJdbc2Trabalho(int idxJdbc) {
		return mapaIdxJdbc2IdxTrabalho[idxJdbc];
	}

	@Override
	public ResultSet executeQuery() throws SQLException {
		return pstmtInternaReal.executeQuery();
	}

	@Override
	public int executeUpdate() throws SQLException {
		return pstmtInternaReal.executeUpdate();
	}

	@Override
	public void setNull(int parameterIndex, int sqlType) {
		throw new UnsupportedOperationException("Não pode chamar o `setNull` internamente");
	}

	@Override
	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		pstmtInternaReal.setBoolean(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setByte(int parameterIndex, byte x) throws SQLException {
		pstmtInternaReal.setByte(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setShort(int parameterIndex, short x) throws SQLException {
		pstmtInternaReal.setShort(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setInt(int parameterIndex, int x) throws SQLException {
		pstmtInternaReal.setInt(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setLong(int parameterIndex, long x) throws SQLException {
		pstmtInternaReal.setLong(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setFloat(int parameterIndex, float x) throws SQLException {
		pstmtInternaReal.setFloat(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setDouble(int parameterIndex, double x) throws SQLException {
		pstmtInternaReal.setDouble(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
		pstmtInternaReal.setBigDecimal(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setString(int parameterIndex, String x) throws SQLException {
		pstmtInternaReal.setString(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		pstmtInternaReal.setBytes(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setDate(int parameterIndex, Date x) throws SQLException {
		pstmtInternaReal.setDate(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setTime(int parameterIndex, Time x) throws SQLException {
		pstmtInternaReal.setTime(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
		pstmtInternaReal.setTimestamp(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
		pstmtInternaReal.setAsciiStream(fromJdbc2Trabalho(parameterIndex), x, length);
	}

	@Override
	public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
		pstmtInternaReal.setUnicodeStream(fromJdbc2Trabalho(parameterIndex), x, length);
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
		pstmtInternaReal.setBinaryStream(fromJdbc2Trabalho(parameterIndex), x, length);
	}

	@Override
	public void clearParameters() throws SQLException {
		pstmtInternaReal.clearParameters();
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
		pstmtInternaReal.setObject(fromJdbc2Trabalho(parameterIndex), x, targetSqlType);
	}

	@Override
	public void setObject(int parameterIndex, Object x) throws SQLException {
		pstmtInternaReal.setObject(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public boolean execute() throws SQLException {
		return pstmtInternaReal.execute();
	}

	@Override
	public void addBatch() throws SQLException {
		pstmtInternaReal.addBatch();
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
		pstmtInternaReal.setCharacterStream(fromJdbc2Trabalho(parameterIndex), reader, length);
	}

	@Override
	public void setRef(int parameterIndex, Ref x) throws SQLException {
		pstmtInternaReal.setRef(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		pstmtInternaReal.setBlob(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setClob(int parameterIndex, Clob x) throws SQLException {
		pstmtInternaReal.setClob(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setArray(int parameterIndex, Array x) throws SQLException {
		pstmtInternaReal.setArray(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		return pstmtInternaReal.getMetaData();
	}

	@Override
	public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
		pstmtInternaReal.setDate(fromJdbc2Trabalho(parameterIndex), x, cal);
	}

	@Override
	public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
		pstmtInternaReal.setTime(fromJdbc2Trabalho(parameterIndex), x, cal);
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
		pstmtInternaReal.setTimestamp(fromJdbc2Trabalho(parameterIndex), x, cal);
	}

	@Override
	public void setNull(int parameterIndex, int sqlType, String typeName) {
		throw new UnsupportedOperationException("Não pode chamar o `setNull` internamente");
	}

	@Override
	public void setURL(int parameterIndex, URL x) throws SQLException {
		pstmtInternaReal.setURL(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException {
		return pstmtInternaReal.getParameterMetaData();
	}

	@Override
	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		pstmtInternaReal.setRowId(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setNString(int parameterIndex, String value) throws SQLException {
		pstmtInternaReal.setNString(fromJdbc2Trabalho(parameterIndex), value);
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
		pstmtInternaReal.setNCharacterStream(fromJdbc2Trabalho(parameterIndex), value, length);
	}

	@Override
	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		pstmtInternaReal.setNClob(fromJdbc2Trabalho(parameterIndex), value);
	}

	@Override
	public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
		pstmtInternaReal.setClob(fromJdbc2Trabalho(parameterIndex), reader, length);
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
		pstmtInternaReal.setBlob(fromJdbc2Trabalho(parameterIndex), inputStream, length);
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
		pstmtInternaReal.setNClob(fromJdbc2Trabalho(parameterIndex), reader, length);
	}

	@Override
	public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
		pstmtInternaReal.setSQLXML(fromJdbc2Trabalho(parameterIndex), xmlObject);
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {
		pstmtInternaReal.setObject(fromJdbc2Trabalho(parameterIndex), x, targetSqlType, scaleOrLength);
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
		pstmtInternaReal.setAsciiStream(fromJdbc2Trabalho(parameterIndex), x, length);
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
		pstmtInternaReal.setBinaryStream(fromJdbc2Trabalho(parameterIndex), x, length);
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
		pstmtInternaReal.setCharacterStream(fromJdbc2Trabalho(parameterIndex), reader, length);
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
		pstmtInternaReal.setAsciiStream(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
		pstmtInternaReal.setBinaryStream(fromJdbc2Trabalho(parameterIndex), x);
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
		pstmtInternaReal.setCharacterStream(fromJdbc2Trabalho(parameterIndex), reader);
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
		pstmtInternaReal.setNCharacterStream(fromJdbc2Trabalho(parameterIndex), value);
	}

	@Override
	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		pstmtInternaReal.setClob(fromJdbc2Trabalho(parameterIndex), reader);
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
		pstmtInternaReal.setBlob(fromJdbc2Trabalho(parameterIndex), inputStream);
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		pstmtInternaReal.setNClob(fromJdbc2Trabalho(parameterIndex), reader);
	}

	@Override
	public ResultSet executeQuery(String sql) throws SQLException {
		return pstmtInternaReal.executeQuery(sql);
	}

	@Override
	public int executeUpdate(String sql) throws SQLException {
		return pstmtInternaReal.executeUpdate(sql);
	}

	@Override
	public void close() throws SQLException {
		pstmtInternaReal.close();
	}

	@Override
	public int getMaxFieldSize() throws SQLException {
		return pstmtInternaReal.getMaxFieldSize();
	}

	@Override
	public void setMaxFieldSize(int max) throws SQLException {
		pstmtInternaReal.setMaxFieldSize(max);
	}

	@Override
	public int getMaxRows() throws SQLException {
		return pstmtInternaReal.getMaxRows();
	}

	@Override
	public void setMaxRows(int max) throws SQLException {
		pstmtInternaReal.setMaxRows(max);
	}

	@Override
	public void setEscapeProcessing(boolean enable) throws SQLException {
		pstmtInternaReal.setEscapeProcessing(enable);
	}

	@Override
	public int getQueryTimeout() throws SQLException {
		return pstmtInternaReal.getQueryTimeout();
	}

	@Override
	public void setQueryTimeout(int seconds) throws SQLException {
		pstmtInternaReal.setQueryTimeout(seconds);
	}

	@Override
	public void cancel() throws SQLException {
		pstmtInternaReal.cancel();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return pstmtInternaReal.getWarnings();
	}

	@Override
	public void clearWarnings() throws SQLException {
		pstmtInternaReal.clearWarnings();
	}

	@Override
	public void setCursorName(String name) throws SQLException {
		pstmtInternaReal.setCursorName(name);
	}

	@Override
	public boolean execute(String sql) throws SQLException {
		return pstmtInternaReal.execute(sql);
	}

	@Override
	public ResultSet getResultSet() throws SQLException {
		return pstmtInternaReal.getResultSet();
	}

	@Override
	public int getUpdateCount() throws SQLException {
		return pstmtInternaReal.getUpdateCount();
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		return pstmtInternaReal.getMoreResults();
	}

	@Override
	public void setFetchDirection(int direction) throws SQLException {
		pstmtInternaReal.setFetchDirection(direction);
	}

	@Override
	public int getFetchDirection() throws SQLException {
		return pstmtInternaReal.getFetchDirection();
	}

	@Override
	public void setFetchSize(int rows) throws SQLException {
		pstmtInternaReal.setFetchSize(rows);
	}

	@Override
	public int getFetchSize() throws SQLException {
		return pstmtInternaReal.getFetchSize();
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		return pstmtInternaReal.getResultSetConcurrency();
	}

	@Override
	public int getResultSetType() throws SQLException {
		return pstmtInternaReal.getResultSetType();
	}

	@Override
	public void addBatch(String sql) throws SQLException {
		pstmtInternaReal.addBatch(sql);
	}

	@Override
	public void clearBatch() throws SQLException {
		pstmtInternaReal.clearBatch();
	}

	@Override
	public int[] executeBatch() throws SQLException {
		return pstmtInternaReal.executeBatch();
	}

	@Override
	public Connection getConnection() throws SQLException {
		return pstmtInternaReal.getConnection();
	}

	@Override
	public boolean getMoreResults(int current) throws SQLException {
		return pstmtInternaReal.getMoreResults(current);
	}

	@Override
	public ResultSet getGeneratedKeys() throws SQLException {
		return pstmtInternaReal.getGeneratedKeys();
	}

	@Override
	public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		return pstmtInternaReal.executeUpdate(sql, autoGeneratedKeys);
	}

	@Override
	public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
		return pstmtInternaReal.executeUpdate(sql, columnIndexes);
	}

	@Override
	public int executeUpdate(String sql, String[] columnNames) throws SQLException {
		return pstmtInternaReal.executeUpdate(sql, columnNames);
	}

	@Override
	public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
		return pstmtInternaReal.execute(sql, autoGeneratedKeys);
	}

	@Override
	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		return pstmtInternaReal.execute(sql, columnIndexes);
	}

	@Override
	public boolean execute(String sql, String[] columnNames) throws SQLException {
		return pstmtInternaReal.execute(sql, columnNames);
	}

	@Override
	public int getResultSetHoldability() throws SQLException {
		return pstmtInternaReal.getResultSetHoldability();
	}

	@Override
	public boolean isClosed() throws SQLException {
		return pstmtInternaReal.isClosed();
	}

	@Override
	public void setPoolable(boolean poolable) throws SQLException {
		pstmtInternaReal.setPoolable(poolable);
	}

	@Override
	public boolean isPoolable() throws SQLException {
		return pstmtInternaReal.isPoolable();
	}

//	@Override
	public void closeOnCompletion() throws SQLException {
//		pstmtInternaReal.closeOnCompletion();
	}

//	@Override
	public boolean isCloseOnCompletion() throws SQLException {
		return false;
//		return pstmtInternaReal.isCloseOnCompletion();
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return pstmtInternaReal.unwrap(iface);
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return pstmtInternaReal.isWrapperFor(iface);
	}
}
