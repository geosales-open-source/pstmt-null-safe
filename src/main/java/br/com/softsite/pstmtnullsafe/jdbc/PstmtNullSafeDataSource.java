package br.com.softsite.pstmtnullsafe.jdbc;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Wrapper;
import java.util.logging.Logger;

import javax.sql.DataSource;

public class PstmtNullSafeDataSource implements DataSource, WrapperHelper {

	private final DataSource underlying;

	public PstmtNullSafeDataSource(DataSource underlying) {
		this.underlying = underlying;
	}

	@Override
	public Wrapper getUnderlying() {
		return underlying;
	}

	@Override
	public PrintWriter getLogWriter() throws SQLException {
		return underlying.getLogWriter();
	}

	@Override
	public void setLogWriter(PrintWriter out) throws SQLException {
		underlying.setLogWriter(out);
	}

	@Override
	public void setLoginTimeout(int seconds) throws SQLException {
		underlying.setLoginTimeout(seconds);
	}

	@Override
	public int getLoginTimeout() throws SQLException {
		return underlying.getLoginTimeout();
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return underlying.getParentLogger();
	}

	@Override
	public Connection getConnection() throws SQLException {
		return new PstmtNullSafeConnection(underlying.getConnection());
	}

	@Override
	public Connection getConnection(String username, String password) throws SQLException {
		return new PstmtNullSafeConnection(underlying.getConnection(username, password));
	}

	@Override
	public String toString() {
		return super.toString() + " over " + underlying;
	}
}
