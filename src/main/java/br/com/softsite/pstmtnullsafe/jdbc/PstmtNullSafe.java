package br.com.softsite.pstmtnullsafe.jdbc;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

import br.com.softsite.pstmtnullsafe.SQLRuntimeException;
import br.com.softsite.pstmtnullsafe.function.SQLConsumer;
import br.com.softsite.pstmtnullsafe.function.SQLFunction;
import br.com.softsite.pstmtnullsafe.function.SQLTriConsumer;
import br.com.softsite.pstmtnullsafe.resource.Pool;
import br.com.softsite.toolbox.indirection.IntIndirection;

public class PstmtNullSafe implements PreparedStatement {

	private final String rawQuery;
	private final PstmtNullSafeConnection conn;
	private final List<String> parts;
	private final int questCount;

	private final PreparedStatement allNonNullQuery;
	private final Pool<NuloIds, PstmtImplNoNulls> poolPstmt;
	private PreparedStatement lastQuery;
	private final HashMap<String, SQLConsumer<PreparedStatement>> lazyPreparer = new HashMap<>();
	private final SQLConsumer<PreparedStatement>[] paramPreparador;
	private final int[] nulos;
	private boolean closed = false;

	private static final SQLConsumer<PreparedStatement> EMPTY_SQL_CONSUMER = __ -> {};
	@Override
	public String toString() {
		return rawQuery;
	}

	PstmtNullSafe(String rawQuery, SQLFunction<String, PreparedStatement> ultimatePreparer, Function<Function<NuloIds, PstmtImplNoNulls>, Pool<NuloIds, PstmtImplNoNulls>> poolCreator, PstmtNullSafeConnection conn) {
		this(rawQuery, preproccess(rawQuery), ultimatePreparer, poolCreator, conn);
	}

	private PstmtNullSafe(String rawQuery, List<String> parts, SQLFunction<String, PreparedStatement> ultimatePreparer, Function<Function<NuloIds, PstmtImplNoNulls>, Pool<NuloIds, PstmtImplNoNulls>> poolCreator, PstmtNullSafeConnection conn) {
		this.conn = conn;
		this.rawQuery = rawQuery;

		this.parts = Collections.unmodifiableList(new ArrayList<>(parts));
		this.questCount = parts.size() - 1;
		this.paramPreparador = new SQLConsumer[questCount + 1];
		this.nulos = new int[questCount/32 + 1];

		Arrays.fill(paramPreparador, EMPTY_SQL_CONSUMER);
		lazyPreparer.put("setParam", pstmt -> {
			for (int i = 1; i <= questCount; i++) {
				paramPreparador[i].accept(pstmt);
			}
		});
		poolPstmt = poolCreator.apply(nulos -> createPstmtImpl(nulos, ultimatePreparer));
		allNonNullQuery = poolPstmt.getResource(new NuloIds(nulos));
		poolPstmt.releaseAll();
	}

	private PstmtImplNoNulls createPstmtImpl(NuloIds nulos, SQLFunction<String, PreparedStatement> ultimatePreparer) {
		return new PstmtImplNoNulls(nulos, questCount, ultimatePreparer.toFunction().apply(createQueryString(nulos)));
	}

	private String createQueryString(NuloIds nulos) {
		StringBuilder buff = new StringBuilder(parts.get(0));

		for (int i = 1; i <= questCount; i++) {
			if (nulos.isNull(i)) {
				buff.append(" null ");
			} else {
				buff.append("?");
			}
			buff.append(parts.get(i));
		}
		return buff.toString();
	}

	private NuloIds getNulosId() {
		return new NuloIds(nulos);
	}

	private void addQueryIfNotExists() throws SQLException {
		NuloIds nulos = getNulosId();
		PreparedStatement pstmt = poolPstmt.getResource(nulos);
		setLastQuery(pstmt);
	}

	private void setLastQuery(PreparedStatement query) {
		lastQuery = query;

		lazyPreparer.values().stream().map(SQLConsumer::toConsumer).forEach(p -> p.accept(query));
	}

	private static List<String> preproccess(String rawQuery) {
		ArrayList<String> parts = new ArrayList<>();
		StringBuilder buff = new StringBuilder();
		IntIndirection ind = new IntIndirection(0);

		rawQuery.chars().forEachOrdered(c -> {
			int state = ind.x;
			switch (c) {
			case '\'':
				if (state == 0) {
					ind.x = 1;
				} else if (state == 1) {
					ind.x = 2;
				} else if (state == 2) {
					ind.x = 1;
				}
				buff.append((char) c);
				break;
			case '?':
				if (state == 1) { // dentro das aspas
					buff.append((char) c);
				} else { // ? solta, logo parâmetro
					if (state == 2) {
						ind.x = 0;
					}
					String sub = buff.toString();
					parts.add(sub);
					buff.setLength(0);
				}
				break;
			default:
				if (state == 2) {
					ind.x = 0;
				}
				buff.append((char) c);
			}
		});
		if (buff.length() != 0) {
			parts.add(buff.toString());
		} else {
			parts.add("");
		}
		return parts;
	}

	@Override
	public ResultSet executeQuery(String sql) throws SQLException {
		throw new SQLException("Operação não suportada");
	}

	@Override
	public int executeUpdate(String sql) throws SQLException {
		throw new SQLException("Operação não suportada");
	}

	@Override
	public void close() throws SQLException {
		allRun(PreparedStatement::close, poolPstmt.getAllResources());
		closed = true;
	}

	@Override
	public int getMaxFieldSize() throws SQLException {
		return allNonNullQuery.getMaxFieldSize();
	}

	@Override
	public void setMaxFieldSize(int max) throws SQLException {
		putPreparer("setMaxFieldSize", pstmt -> pstmt.setMaxFieldSize(max));
	}

	@Override
	public int getMaxRows() throws SQLException {
		return allNonNullQuery.getMaxRows();
	}

	@Override
	public void setMaxRows(int max) throws SQLException {
		putPreparer("setMaxRows", pstmt -> pstmt.setMaxRows(max));
	}

	@Override
	public void setEscapeProcessing(boolean enable) throws SQLException {
		putPreparer("setEscapeProcessing", pstmt -> pstmt.setEscapeProcessing(enable));
	}

	@Override
	public int getQueryTimeout() throws SQLException {
		return allNonNullQuery.getQueryTimeout();
	}

	@Override
	public void setQueryTimeout(int seconds) throws SQLException {
		putPreparer("setMaxRows", pstmt -> pstmt.setQueryTimeout(seconds));
	}

	@Override
	public void cancel() throws SQLException {
		allRun(PreparedStatement::cancel);
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return lastQuery.getWarnings();
	}

	@Override
	public void clearWarnings() throws SQLException {
		allRun(PreparedStatement::clearWarnings);
	}

	@Override
	public void setCursorName(String name) throws SQLException {
		putPreparer("setCursorName", pstmt -> pstmt.setCursorName(name));
	}

	@Override
	public boolean execute(String sql) throws SQLException {
		throw new SQLException("Operação não suportada");
	}

	@Override
	public ResultSet getResultSet() throws SQLException {
		return lastQuery.getResultSet();
	}

	@Override
	public int getUpdateCount() throws SQLException {
		return lastQuery.getUpdateCount();
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		return lastQuery.getMoreResults();
	}

	@Override
	public void setFetchDirection(int direction) throws SQLException {
		putPreparer("setFetchDirection", pstmt -> pstmt.setFetchDirection(direction));
	}

	@Override
	public int getFetchDirection() throws SQLException {
		return allNonNullQuery.getFetchDirection();
	}

	@Override
	public void setFetchSize(int rows) throws SQLException {
		putPreparer("setFetchSize", pstmt -> pstmt.setFetchSize(rows));
	}

	@Override
	public int getFetchSize() throws SQLException {
		return allNonNullQuery.getFetchSize();
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		return allNonNullQuery.getResultSetConcurrency();
	}

	@Override
	public int getResultSetType() throws SQLException {
		return allNonNullQuery.getResultSetType();
	}

	@Override
	public void addBatch(String sql) throws SQLException {
		throw new SQLException("Operação não suportada");
	}

	@Override
	public void clearBatch() throws SQLException {
		allRun(PreparedStatement::clearBatch);
	}

	@Override
	public int[] executeBatch() throws SQLException {
		try {
			return poolPstmt.getAllocatedResources().stream()
					.map(SQLFunction.staticToFunction(PreparedStatement::executeBatch))
					.flatMapToInt(IntStream::of)
					.toArray();
		} finally {
			poolPstmt.releaseAll();
		}
	}

	@Override
	public Connection getConnection() throws SQLException {
		return conn;
	}

	@Override
	public boolean getMoreResults(int current) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ResultSet getGeneratedKeys() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		throw new SQLException("Operação não suportada");
	}

	@Override
	public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
		throw new SQLException("Operação não suportada");
	}

	@Override
	public int executeUpdate(String sql, String[] columnNames) throws SQLException {
		throw new SQLException("Operação não suportada");
	}

	@Override
	public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
		throw new SQLException("Operação não suportada");
	}

	@Override
	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		throw new SQLException("Operação não suportada");
	}

	@Override
	public boolean execute(String sql, String[] columnNames) throws SQLException {
		throw new SQLException("Operação não suportada");
	}

	@Override
	public int getResultSetHoldability() throws SQLException {
		return allNonNullQuery.getResultSetHoldability();
	}

	@Override
	public boolean isClosed() throws SQLException {
		return closed || allNonNullQuery.isClosed();
	}

	@Override
	public void setPoolable(boolean poolable) throws SQLException {
		putPreparer("setPoolable", pstmt -> pstmt.setPoolable(poolable));
	}

	@Override
	public boolean isPoolable() throws SQLException {
		return allNonNullQuery.isPoolable();
	}

//	@Override
	public void closeOnCompletion() throws SQLException {
//		putPreparer("closeOnCompletion", PreparedStatement::closeOnCompletion);
	}

//	@Override
	public boolean isCloseOnCompletion() throws SQLException {
		return false;
//		return allNonNullQuery.isCloseOnCompletion();
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		if (iface == PstmtNullSafe.class) {
			return iface.cast(this);
		}
		throw new SQLException(getClass() + " does not wrap " + iface);
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return iface == PstmtNullSafe.class;
	}

	@Override
	public ResultSet executeQuery() throws SQLException {
		addQueryIfNotExists();
		return WrapperUtils.getResultSetProxy4PstmtNullSafe(lastQuery.executeQuery(), this);
	}

	@Override
	public int executeUpdate() throws SQLException {
		addQueryIfNotExists();
		return lastQuery.executeUpdate();
	}

	private <T> void setParamPreparador(T obj, int parameterIndex, SQLTriConsumer<PreparedStatement, Integer, T> preparador) {
		if (obj == null) {
			NuloIds.setNull(nulos, parameterIndex);
			paramPreparador[parameterIndex] = EMPTY_SQL_CONSUMER;
		} else {
			NuloIds.setNonNull(nulos, parameterIndex);
			paramPreparador[parameterIndex] = pstmt -> preparador.accept(pstmt, parameterIndex, obj);
		}
	}

	private void setNull(int parameterIndex) {
		setParamPreparador(null, parameterIndex, (pstmt, __, ___) -> {});
	}

	@Override
	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		setNull(parameterIndex);
	}

	@Override
	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setBoolean);
	}

	@Override
	public void setByte(int parameterIndex, byte x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setByte);
	}

	@Override
	public void setShort(int parameterIndex, short x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setShort);
	}

	@Override
	public void setInt(int parameterIndex, int x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setInt);
	}

	@Override
	public void setLong(int parameterIndex, long x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setLong);
	}

	@Override
	public void setFloat(int parameterIndex, float x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setFloat);
	}

	@Override
	public void setDouble(int parameterIndex, double x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setDouble);
	}

	@Override
	public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setBigDecimal);
	}

	@Override
	public void setString(int parameterIndex, String x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setString);
	}

	@Override
	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setBytes);
	}

	@Override
	public void setDate(int parameterIndex, Date x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setDate);
	}

	@Override
	public void setTime(int parameterIndex, Time x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setTime);
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setTimestamp);
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
		setParamPreparador(x, parameterIndex, (pstmt, idx, __) -> pstmt.setAsciiStream(idx, x, length));
	}

	@Override
	@Deprecated
	@SuppressWarnings("deprecation")
	public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
		setParamPreparador(x, parameterIndex, (pstmt, idx, __) -> pstmt.setUnicodeStream(idx, x, length));
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
		setParamPreparador(x, parameterIndex, (pstmt, idx, __) -> pstmt.setBinaryStream(idx, x, length));
	}

	@Override
	public void clearParameters() throws SQLException {
		Arrays.fill(paramPreparador, EMPTY_SQL_CONSUMER);
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
		setParamPreparador(x, parameterIndex, (pstmt, idx, __) -> pstmt.setObject(idx, x, targetSqlType));
	}

	@Override
	public void setObject(int parameterIndex, Object x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setObject);
	}

	@Override
	public boolean execute() throws SQLException {
		addQueryIfNotExists();
		return lastQuery.execute();
	}

	@Override
	public void addBatch() throws SQLException {
		addQueryIfNotExists();
		lastQuery.addBatch();
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
		setParamPreparador(reader, parameterIndex, (pstmt, idx, __) -> pstmt.setCharacterStream(idx, reader, length));
	}

	@Override
	public void setRef(int parameterIndex, Ref x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setRef);
	}

	@Override
	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setBlob);
	}

	@Override
	public void setClob(int parameterIndex, Clob x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setClob);
	}

	@Override
	public void setArray(int parameterIndex, Array x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setArray);
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		return allNonNullQuery.getMetaData();
	}

	@Override
	public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
		setParamPreparador(x, parameterIndex, (pstmt, idx, __) -> pstmt.setDate(idx, x, cal));
	}

	@Override
	public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
		setParamPreparador(x, parameterIndex, (pstmt, idx, __) -> pstmt.setTime(idx, x, cal));
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
		setParamPreparador(x, parameterIndex, (pstmt, idx, __) -> pstmt.setTimestamp(idx, x, cal));
	}

	@Override
	public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
		setNull(parameterIndex);
	}

	@Override
	public void setURL(int parameterIndex, URL x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setURL);
	}

	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException {
		return allNonNullQuery.getParameterMetaData();
	}

	@Override
	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		putPreparer("setRowId", pstmt -> pstmt.setRowId(parameterIndex, x));
	}

	@Override
	public void setNString(int parameterIndex, String value) throws SQLException {
		setParamPreparador(value, parameterIndex, PreparedStatement::setNString);
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
		setParamPreparador(value, parameterIndex, (pstmt, idx, __) -> pstmt.setNCharacterStream(idx, value, length));
	}

	@Override
	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		setParamPreparador(value, parameterIndex, PreparedStatement::setNClob);
	}

	@Override
	public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
		setParamPreparador(reader, parameterIndex, (pstmt, idx, __) -> pstmt.setClob(idx, reader, length));
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
		setParamPreparador(inputStream, parameterIndex, (pstmt, idx, __) -> pstmt.setBlob(idx, inputStream, length));
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
		setParamPreparador(reader, parameterIndex, (pstmt, idx, __) -> pstmt.setNClob(idx, reader, length));
	}

	@Override
	public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
		setParamPreparador(xmlObject, parameterIndex, PreparedStatement::setSQLXML);
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {
		setParamPreparador(x, parameterIndex, (pstmt, idx, __) -> pstmt.setObject(idx, x, targetSqlType, scaleOrLength));
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
		setParamPreparador(x, parameterIndex, (pstmt, idx, __) -> pstmt.setAsciiStream(idx, x, length));
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
		setParamPreparador(x, parameterIndex, (pstmt, idx, __) -> pstmt.setBinaryStream(idx, x, length));
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
		setParamPreparador(reader, parameterIndex, (pstmt, idx, __) -> pstmt.setCharacterStream(idx, reader, length));
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setAsciiStream);
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
		setParamPreparador(x, parameterIndex, PreparedStatement::setBinaryStream);
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
		setParamPreparador(reader, parameterIndex, PreparedStatement::setCharacterStream);
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
		setParamPreparador(value, parameterIndex, PreparedStatement::setCharacterStream);
	}

	@Override
	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		setParamPreparador(reader, parameterIndex, PreparedStatement::setCharacterStream);
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
		setParamPreparador(inputStream, parameterIndex, PreparedStatement::setBlob);
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		setParamPreparador(reader, parameterIndex, PreparedStatement::setNClob);
	}

	private void putPreparer(String id, SQLConsumer<PreparedStatement> preparer) throws SQLException {
		try {
			preparer.accept(allNonNullQuery);
		} catch (SQLRuntimeException e) {
			throw e.getInnerException();
		}
		lazyPreparer.put(id, preparer);
	}

	private void allRun(SQLConsumer<PreparedStatement> consumer) throws SQLException {
		allRun(consumer, poolPstmt.getAllocatedResources());
	}

	private void allRun(SQLConsumer<PreparedStatement> consumer, List<? extends PreparedStatement> stmts) throws SQLException {
		SQLException sqle = null;
		for (PreparedStatement pstmt: stmts) {
			try {
				consumer.accept(pstmt);
			} catch (SQLException e) {
				if (sqle == null) {
					sqle = e;
				} else {
					sqle.addSuppressed(e);
				}
			}
		}
		if (sqle != null) {
			throw sqle;
		}
	}
}
