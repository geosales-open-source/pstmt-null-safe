package br.com.softsite.pstmtnullsafe.jdbc;

import java.sql.SQLException;
import java.sql.Wrapper;

interface WrapperHelper extends Wrapper {

	Wrapper getUnderlying();

	@SuppressWarnings("unchecked")
	@Override
	default <T> T unwrap(Class<T> iface) throws SQLException {
		if (isWrapperFor(iface)) {
			if (iface.isAssignableFrom(this.getClass())) {
				return (T) this;
			}
			return getUnderlying().unwrap(iface);
		}
		throw new SQLException(getClass() + " (and underlying " + getUnderlying().getClass() + ") does not wrap " + iface);
	}

	@Override
	default boolean isWrapperFor(Class<?> iface) throws SQLException {
		return iface.isAssignableFrom(this.getClass()) || getUnderlying().isWrapperFor(iface);
	}
}
