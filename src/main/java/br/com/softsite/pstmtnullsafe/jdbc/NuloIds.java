package br.com.softsite.pstmtnullsafe.jdbc;

import java.util.Arrays;

class NuloIds {

	private static final int SIZE = 32;
	final int[] nulos;
	private final int hash;

	NuloIds(int[] nulos) {
		this.nulos = new int[nulos.length];
		System.arraycopy(nulos, 0, this.nulos, 0, nulos.length);
		if (nulos.length > 0) {
			int hash = nulos[0];
			for (int i = 1; i < nulos.length; i++) {
				hash = hash ^ nulos[i];
			}
			this.hash = hash;
		} else {
			this.hash = 0;
		}
	}

	static void setNonNull(int[] nulos, int idx) {
		int idxArray = idx/SIZE;
		nulos[idxArray] |= 1 <<(idx % SIZE);
	}

	static void setNull(int[] nulos, int idx) {
		int idxArray = idx/SIZE;
		nulos[idxArray] &= ~(1 <<(idx % SIZE));
	}

	boolean isNull(int idx) {
		int idxArray = idx/SIZE;
		return (nulos[idxArray] & (1 <<(idx % SIZE))) == 0;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		NuloIds nuloIds = (NuloIds) o;
		return hash == nuloIds.hash && Arrays.equals(nulos, nuloIds.nulos);
	}

	@Override
	public int hashCode() {
		return hash;
	}
}
