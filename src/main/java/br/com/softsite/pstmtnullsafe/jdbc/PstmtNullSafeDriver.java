package br.com.softsite.pstmtnullsafe.jdbc;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

public class PstmtNullSafeDriver implements Driver {

	public static final String PREFIX_URL = "jdbc:pstmt-nullsafe:";
	public static final PstmtNullSafeDriver instance;

	static {
		instance = new PstmtNullSafeDriver();
		try {
			DriverManager.registerDriver(instance);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Connection connect(String url, Properties info) throws SQLException {
		String underlyingUrl = toUnderlyingUrl(url);
		Driver underlying = getUnderlyingDriverFromUnderlyingUrl(underlyingUrl);
		return new PstmtNullSafeConnection(underlying.connect(underlyingUrl, info));
	}

	@Override
	public boolean acceptsURL(String url) throws SQLException {
		if (url.startsWith(PREFIX_URL)) {
			try {
				return getUnderlyingDriver(url) != null;
			} catch (SQLException e) {
				if ("08001".equals(e.getSQLState())) {
					return false;
				} else {
					throw e;
				}
			}
		}
		return false;
	}

	@Override
	public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
		String underlyingUrl = toUnderlyingUrl(url);
		Driver underlying = getUnderlyingDriverFromUnderlyingUrl(underlyingUrl);
		return underlying.getPropertyInfo(underlyingUrl, info);
	}

	@Override
	public int getMajorVersion() {
		return 1;
	}

	@Override
	public int getMinorVersion() {
		return 0;
	}

	@Override
	public boolean jdbcCompliant() {
		return false;
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		throw new SQLFeatureNotSupportedException("getParentLogger not supported");
	}

	private String toUnderlyingUrl(String url) {
		return "jdbc:" + url.substring(PREFIX_URL.length());
	}

	private Driver getUnderlyingDriver(String url) throws SQLException {
		return DriverManager.getDriver(toUnderlyingUrl(url));
	}

	private Driver getUnderlyingDriverFromUnderlyingUrl(String underlyingUrl) throws SQLException {
		return DriverManager.getDriver(underlyingUrl);
	}
}
