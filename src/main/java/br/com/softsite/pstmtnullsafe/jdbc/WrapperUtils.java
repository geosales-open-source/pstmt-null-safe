package br.com.softsite.pstmtnullsafe.jdbc;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

final class WrapperUtils {

	private WrapperUtils() {
	}

	static ResultSet getResultSetProxy4PstmtNullSafe(ResultSet resultSet, PstmtNullSafe pstmt) {
		return (ResultSet) Proxy.newProxyInstance(pstmt.getClass().getClassLoader(), new Class[] { ResultSet.class },
				(Object proxy, Method method, Object[] args) -> {
					if ("getStatement".equals(method.getName())) {
						return pstmt;
					}
					return method.invoke(resultSet, args);
				});
	}

	static Statement getStatement(Statement stmt, PstmtNullSafeConnection conn) {
		return (Statement) Proxy.newProxyInstance(conn.getClass().getClassLoader(), new Class[] { Statement.class },
				(Object proxy, Method method, Object[] args) -> {
					switch (method.getName()) {
					case "getConnection":
						return conn;
					case "executeQuery":
					case "getGeneratedKeys":
					case "getResultSet": {
						ResultSet rs = (ResultSet) method.invoke(stmt, args);
						return getResultSet4Statement(rs, (Statement) proxy);
					}
					}
					return method.invoke(stmt, args);
				});
	}

	static ResultSet getResultSet4Statement(ResultSet resultSet, Statement stmt) {
		return (ResultSet) Proxy.newProxyInstance(stmt.getClass().getClassLoader(), new Class[] { ResultSet.class },
				(Object proxy, Method method, Object[] args) -> {
					if ("getStatement".equals(method.getName())) {
						return stmt;
					}
					return method.invoke(resultSet, args);
				});
	}

	static ResultSet getResultSet4ThinAir(ResultSet resultSet, PstmtNullSafeConnection conn) throws SQLException {
		Statement stmt = getStatement(resultSet.getStatement(), conn);
		return getResultSet4Statement(resultSet, stmt);
	}

}
