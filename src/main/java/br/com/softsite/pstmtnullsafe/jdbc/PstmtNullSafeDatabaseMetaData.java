package br.com.softsite.pstmtnullsafe.jdbc;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.RowIdLifetime;
import java.sql.SQLException;
import java.sql.Wrapper;

public class PstmtNullSafeDatabaseMetaData implements DatabaseMetaData, WrapperHelper {

	private final DatabaseMetaData underlying;
	private final PstmtNullSafeConnection conn;

	public PstmtNullSafeDatabaseMetaData(DatabaseMetaData underlying, PstmtNullSafeConnection conn) {
		this.underlying = underlying;
		this.conn = conn;
	}

	@Override
	public Wrapper getUnderlying() {
		return underlying;
	}

	@Override
	public boolean allProceduresAreCallable() throws SQLException {
		return underlying.allProceduresAreCallable();
	}

	@Override
	public boolean allTablesAreSelectable() throws SQLException {
		return underlying.allProceduresAreCallable();
	}

	@Override
	public String getURL() throws SQLException {
		return PstmtNullSafeDriver.PREFIX_URL + underlying.getURL();
	}

	@Override
	public String getUserName() throws SQLException {
		return underlying.getUserName();
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		return underlying.isReadOnly();
	}

	@Override
	public boolean nullsAreSortedHigh() throws SQLException {
		return underlying.nullsAreSortedHigh();
	}

	@Override
	public boolean nullsAreSortedLow() throws SQLException {
		return underlying.nullsAreSortedLow();
	}

	@Override
	public boolean nullsAreSortedAtStart() throws SQLException {
		return underlying.nullsAreSortedAtStart();
	}

	@Override
	public boolean nullsAreSortedAtEnd() throws SQLException {
		return underlying.nullsAreSortedAtEnd();
	}

	@Override
	public String getDatabaseProductName() throws SQLException {
		return underlying.getDatabaseProductName() + " with pstmt null safe";
	}

	@Override
	public String getDatabaseProductVersion() throws SQLException {
		return underlying.getDatabaseProductVersion();
	}

	@Override
	public String getDriverName() throws SQLException {
		return underlying.getDriverName() + " with pstmt null safe";
	}

	@Override
	public String getDriverVersion() throws SQLException {
		return underlying.getDriverVersion();
	}

	@Override
	public int getDriverMajorVersion() {
		return underlying.getDriverMajorVersion();
	}

	@Override
	public int getDriverMinorVersion() {
		return underlying.getDriverMinorVersion();
	}

	@Override
	public boolean usesLocalFiles() throws SQLException {
		return underlying.usesLocalFiles();
	}

	@Override
	public boolean usesLocalFilePerTable() throws SQLException {
		return underlying.usesLocalFilePerTable();
	}

	@Override
	public boolean supportsMixedCaseIdentifiers() throws SQLException {
		return underlying.supportsMixedCaseIdentifiers();
	}

	@Override
	public boolean storesUpperCaseIdentifiers() throws SQLException {
		return underlying.storesUpperCaseIdentifiers();
	}

	@Override
	public boolean storesLowerCaseIdentifiers() throws SQLException {
		return underlying.storesLowerCaseIdentifiers();
	}

	@Override
	public boolean storesMixedCaseIdentifiers() throws SQLException {
		return underlying.storesMixedCaseIdentifiers();
	}

	@Override
	public boolean supportsMixedCaseQuotedIdentifiers() throws SQLException {
		return underlying.supportsMixedCaseQuotedIdentifiers();
	}

	@Override
	public boolean storesUpperCaseQuotedIdentifiers() throws SQLException {
		return underlying.storesUpperCaseQuotedIdentifiers();
	}

	@Override
	public boolean storesLowerCaseQuotedIdentifiers() throws SQLException {
		return underlying.storesLowerCaseQuotedIdentifiers();
	}

	@Override
	public boolean storesMixedCaseQuotedIdentifiers() throws SQLException {
		return underlying.storesMixedCaseQuotedIdentifiers();
	}

	@Override
	public String getIdentifierQuoteString() throws SQLException {
		return underlying.getIdentifierQuoteString();
	}

	@Override
	public String getSQLKeywords() throws SQLException {
		return underlying.getSQLKeywords();
	}

	@Override
	public String getNumericFunctions() throws SQLException {
		return underlying.getNumericFunctions();
	}

	@Override
	public String getStringFunctions() throws SQLException {
		return underlying.getStringFunctions();
	}

	@Override
	public String getSystemFunctions() throws SQLException {
		return underlying.getSystemFunctions();
	}

	@Override
	public String getTimeDateFunctions() throws SQLException {
		return underlying.getTimeDateFunctions();
	}

	@Override
	public String getSearchStringEscape() throws SQLException {
		return underlying.getSearchStringEscape();
	}

	@Override
	public String getExtraNameCharacters() throws SQLException {
		return underlying.getExtraNameCharacters();
	}

	@Override
	public boolean supportsAlterTableWithAddColumn() throws SQLException {
		return underlying.supportsAlterTableWithAddColumn();
	}

	@Override
	public boolean supportsAlterTableWithDropColumn() throws SQLException {
		return underlying.supportsAlterTableWithDropColumn();
	}

	@Override
	public boolean supportsColumnAliasing() throws SQLException {
		return underlying.supportsColumnAliasing();
	}

	@Override
	public boolean nullPlusNonNullIsNull() throws SQLException {
		return underlying.nullPlusNonNullIsNull();
	}

	@Override
	public boolean supportsConvert() throws SQLException {
		return underlying.supportsConvert();
	}

	@Override
	public boolean supportsConvert(int fromType, int toType) throws SQLException {
		return underlying.supportsConvert(fromType, toType);
	}

	@Override
	public boolean supportsTableCorrelationNames() throws SQLException {
		return underlying.supportsTableCorrelationNames();
	}

	@Override
	public boolean supportsDifferentTableCorrelationNames() throws SQLException {
		return underlying.supportsDifferentTableCorrelationNames();
	}

	@Override
	public boolean supportsExpressionsInOrderBy() throws SQLException {
		return underlying.supportsExpressionsInOrderBy();
	}

	@Override
	public boolean supportsOrderByUnrelated() throws SQLException {
		return underlying.supportsOrderByUnrelated();
	}

	@Override
	public boolean supportsGroupBy() throws SQLException {
		return underlying.supportsGroupBy();
	}

	@Override
	public boolean supportsGroupByUnrelated() throws SQLException {
		return underlying.supportsGroupByUnrelated();
	}

	@Override
	public boolean supportsGroupByBeyondSelect() throws SQLException {
		return underlying.supportsGroupByBeyondSelect();
	}

	@Override
	public boolean supportsLikeEscapeClause() throws SQLException {
		return underlying.supportsLikeEscapeClause();
	}

	@Override
	public boolean supportsMultipleResultSets() throws SQLException {
		return underlying.supportsMultipleResultSets();
	}

	@Override
	public boolean supportsMultipleTransactions() throws SQLException {
		return underlying.supportsMultipleTransactions();
	}

	@Override
	public boolean supportsNonNullableColumns() throws SQLException {
		return underlying.supportsNonNullableColumns();
	}

	@Override
	public boolean supportsMinimumSQLGrammar() throws SQLException {
		return underlying.supportsMinimumSQLGrammar();
	}

	@Override
	public boolean supportsCoreSQLGrammar() throws SQLException {
		return underlying.supportsCoreSQLGrammar();
	}

	@Override
	public boolean supportsExtendedSQLGrammar() throws SQLException {
		return underlying.supportsExtendedSQLGrammar();
	}

	@Override
	public boolean supportsANSI92EntryLevelSQL() throws SQLException {
		return underlying.supportsANSI92EntryLevelSQL();
	}

	@Override
	public boolean supportsANSI92IntermediateSQL() throws SQLException {
		return underlying.supportsANSI92IntermediateSQL();
	}

	@Override
	public boolean supportsANSI92FullSQL() throws SQLException {
		return underlying.supportsANSI92FullSQL();
	}

	@Override
	public boolean supportsIntegrityEnhancementFacility() throws SQLException {
		return underlying.supportsIntegrityEnhancementFacility();
	}

	@Override
	public boolean supportsOuterJoins() throws SQLException {
		return underlying.supportsOuterJoins();
	}

	@Override
	public boolean supportsFullOuterJoins() throws SQLException {
		return underlying.supportsFullOuterJoins();
	}

	@Override
	public boolean supportsLimitedOuterJoins() throws SQLException {
		return underlying.supportsLimitedOuterJoins();
	}

	@Override
	public String getSchemaTerm() throws SQLException {
		return underlying.getSchemaTerm();
	}

	@Override
	public String getProcedureTerm() throws SQLException {
		return underlying.getProcedureTerm();
	}

	@Override
	public String getCatalogTerm() throws SQLException {
		return underlying.getCatalogTerm();
	}

	@Override
	public boolean isCatalogAtStart() throws SQLException {
		return underlying.isCatalogAtStart();
	}

	@Override
	public String getCatalogSeparator() throws SQLException {
		return underlying.getCatalogSeparator();
	}

	@Override
	public boolean supportsSchemasInDataManipulation() throws SQLException {
		return underlying.supportsSchemasInDataManipulation();
	}

	@Override
	public boolean supportsSchemasInProcedureCalls() throws SQLException {
		return underlying.supportsSchemasInProcedureCalls();
	}

	@Override
	public boolean supportsSchemasInTableDefinitions() throws SQLException {
		return underlying.supportsSchemasInTableDefinitions();
	}

	@Override
	public boolean supportsSchemasInIndexDefinitions() throws SQLException {
		return underlying.supportsSchemasInIndexDefinitions();
	}

	@Override
	public boolean supportsSchemasInPrivilegeDefinitions() throws SQLException {
		return underlying.supportsSchemasInPrivilegeDefinitions();
	}

	@Override
	public boolean supportsCatalogsInDataManipulation() throws SQLException {
		return underlying.supportsCatalogsInDataManipulation();
	}

	@Override
	public boolean supportsCatalogsInProcedureCalls() throws SQLException {
		return underlying.supportsCatalogsInProcedureCalls();
	}

	@Override
	public boolean supportsCatalogsInTableDefinitions() throws SQLException {
		return underlying.supportsCatalogsInTableDefinitions();
	}

	@Override
	public boolean supportsCatalogsInIndexDefinitions() throws SQLException {
		return underlying.supportsCatalogsInIndexDefinitions();
	}

	@Override
	public boolean supportsCatalogsInPrivilegeDefinitions() throws SQLException {
		return underlying.supportsCatalogsInPrivilegeDefinitions();
	}

	@Override
	public boolean supportsPositionedDelete() throws SQLException {
		return underlying.supportsPositionedDelete();
	}

	@Override
	public boolean supportsPositionedUpdate() throws SQLException {
		return underlying.supportsPositionedUpdate();
	}

	@Override
	public boolean supportsSelectForUpdate() throws SQLException {
		return underlying.supportsSelectForUpdate();
	}

	@Override
	public boolean supportsStoredProcedures() throws SQLException {
		return underlying.supportsStoredProcedures();
	}

	@Override
	public boolean supportsSubqueriesInComparisons() throws SQLException {
		return underlying.supportsSubqueriesInComparisons();
	}

	@Override
	public boolean supportsSubqueriesInExists() throws SQLException {
		return underlying.supportsSubqueriesInExists();
	}

	@Override
	public boolean supportsSubqueriesInIns() throws SQLException {
		return underlying.supportsSubqueriesInIns();
	}

	@Override
	public boolean supportsSubqueriesInQuantifieds() throws SQLException {
		return underlying.supportsSubqueriesInQuantifieds();
	}

	@Override
	public boolean supportsCorrelatedSubqueries() throws SQLException {
		return underlying.supportsCorrelatedSubqueries();
	}

	@Override
	public boolean supportsUnion() throws SQLException {
		return underlying.supportsUnion();
	}

	@Override
	public boolean supportsUnionAll() throws SQLException {
		return underlying.supportsUnionAll();
	}

	@Override
	public boolean supportsOpenCursorsAcrossCommit() throws SQLException {
		return underlying.supportsOpenCursorsAcrossCommit();
	}

	@Override
	public boolean supportsOpenCursorsAcrossRollback() throws SQLException {
		return underlying.supportsOpenCursorsAcrossRollback();
	}

	@Override
	public boolean supportsOpenStatementsAcrossCommit() throws SQLException {
		return underlying.supportsOpenStatementsAcrossCommit();
	}

	@Override
	public boolean supportsOpenStatementsAcrossRollback() throws SQLException {
		return underlying.supportsOpenStatementsAcrossRollback();
	}

	@Override
	public int getMaxBinaryLiteralLength() throws SQLException {
		return underlying.getMaxBinaryLiteralLength();
	}

	@Override
	public int getMaxCharLiteralLength() throws SQLException {
		return underlying.getMaxCharLiteralLength();
	}

	@Override
	public int getMaxColumnNameLength() throws SQLException {
		return underlying.getMaxColumnNameLength();
	}

	@Override
	public int getMaxColumnsInGroupBy() throws SQLException {
		return underlying.getMaxColumnsInGroupBy();
	}

	@Override
	public int getMaxColumnsInIndex() throws SQLException {
		return underlying.getMaxColumnsInIndex();
	}

	@Override
	public int getMaxColumnsInOrderBy() throws SQLException {
		return underlying.getMaxColumnsInOrderBy();
	}

	@Override
	public int getMaxColumnsInSelect() throws SQLException {
		return underlying.getMaxColumnsInSelect();
	}

	@Override
	public int getMaxColumnsInTable() throws SQLException {
		return underlying.getMaxColumnsInTable();
	}

	@Override
	public int getMaxConnections() throws SQLException {
		return underlying.getMaxConnections();
	}

	@Override
	public int getMaxCursorNameLength() throws SQLException {
		return underlying.getMaxCursorNameLength();
	}

	@Override
	public int getMaxIndexLength() throws SQLException {
		return underlying.getMaxIndexLength();
	}

	@Override
	public int getMaxSchemaNameLength() throws SQLException {
		return underlying.getMaxSchemaNameLength();
	}

	@Override
	public int getMaxProcedureNameLength() throws SQLException {
		return underlying.getMaxProcedureNameLength();
	}

	@Override
	public int getMaxCatalogNameLength() throws SQLException {
		return underlying.getMaxCatalogNameLength();
	}

	@Override
	public int getMaxRowSize() throws SQLException {
		return underlying.getMaxRowSize();
	}

	@Override
	public boolean doesMaxRowSizeIncludeBlobs() throws SQLException {
		return underlying.doesMaxRowSizeIncludeBlobs();
	}

	@Override
	public int getMaxStatementLength() throws SQLException {
		return underlying.getMaxStatementLength();
	}

	@Override
	public int getMaxStatements() throws SQLException {
		return underlying.getMaxStatements();
	}

	@Override
	public int getMaxTableNameLength() throws SQLException {
		return underlying.getMaxTableNameLength();
	}

	@Override
	public int getMaxTablesInSelect() throws SQLException {
		return underlying.getMaxTablesInSelect();
	}

	@Override
	public int getMaxUserNameLength() throws SQLException {
		return underlying.getMaxUserNameLength();
	}

	@Override
	public int getDefaultTransactionIsolation() throws SQLException {
		return underlying.getDefaultTransactionIsolation();
	}

	@Override
	public boolean supportsTransactions() throws SQLException {
		return underlying.supportsTransactions();
	}

	@Override
	public boolean supportsTransactionIsolationLevel(int level) throws SQLException {
		return underlying.supportsTransactionIsolationLevel(level);
	}

	@Override
	public boolean supportsDataDefinitionAndDataManipulationTransactions() throws SQLException {
		return underlying.supportsDataDefinitionAndDataManipulationTransactions();
	}

	@Override
	public boolean supportsDataManipulationTransactionsOnly() throws SQLException {
		return underlying.supportsDataManipulationTransactionsOnly();
	}

	@Override
	public boolean dataDefinitionCausesTransactionCommit() throws SQLException {
		return underlying.dataDefinitionCausesTransactionCommit();
	}

	@Override
	public boolean dataDefinitionIgnoredInTransactions() throws SQLException {
		return underlying.dataDefinitionIgnoredInTransactions();
	}

	@Override
	public ResultSet getProcedures(String catalog, String schemaPattern, String procedureNamePattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getProcedures(catalog, schemaPattern, procedureNamePattern), conn);
	}

	@Override
	public ResultSet getProcedureColumns(String catalog, String schemaPattern, String procedureNamePattern, String columnNamePattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getProcedureColumns(catalog, schemaPattern, procedureNamePattern, columnNamePattern), conn);
	}

	@Override
	public ResultSet getTables(String catalog, String schemaPattern, String tableNamePattern, String[] types) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getTables(catalog, schemaPattern, tableNamePattern, types), conn);
	}

	@Override
	public ResultSet getSchemas() throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getSchemas(), conn);
	}

	@Override
	public ResultSet getCatalogs() throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getCatalogs(), conn);
	}

	@Override
	public ResultSet getTableTypes() throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getTableTypes(), conn);
	}

	@Override
	public ResultSet getColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getColumns(catalog, schemaPattern, tableNamePattern, columnNamePattern), conn);
	}

	@Override
	public ResultSet getColumnPrivileges(String catalog, String schema, String table, String columnNamePattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getColumnPrivileges(catalog, schema, table, columnNamePattern), conn);
	}

	@Override
	public ResultSet getTablePrivileges(String catalog, String schemaPattern, String tableNamePattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getTablePrivileges(catalog, schemaPattern, tableNamePattern), conn);
	}

	@Override
	public ResultSet getBestRowIdentifier(String catalog, String schema, String table, int scope, boolean nullable) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getBestRowIdentifier(catalog, schema, table, scope, nullable), conn);
	}

	@Override
	public ResultSet getVersionColumns(String catalog, String schema, String table) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getVersionColumns(catalog, schema, table), conn);
	}

	@Override
	public ResultSet getPrimaryKeys(String catalog, String schema, String table) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getPrimaryKeys(catalog, schema, table), conn);
	}

	@Override
	public ResultSet getImportedKeys(String catalog, String schema, String table) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getImportedKeys(catalog, schema, table), conn);
	}

	@Override
	public ResultSet getExportedKeys(String catalog, String schema, String table) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getExportedKeys(catalog, schema, table), conn);
	}

	@Override
	public ResultSet getCrossReference(String parentCatalog, String parentSchema, String parentTable, String foreignCatalog, String foreignSchema, String foreignTable) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getCrossReference(parentCatalog, parentSchema, parentTable, foreignCatalog, foreignSchema, foreignTable), conn);
	}

	@Override
	public ResultSet getTypeInfo() throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getTypeInfo(), conn);
	}

	@Override
	public ResultSet getIndexInfo(String catalog, String schema, String table, boolean unique, boolean approximate) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getIndexInfo(catalog, schema, table, unique, approximate), conn);
	}

	@Override
	public boolean supportsResultSetType(int type) throws SQLException {
		return underlying.supportsResultSetType(type);
	}

	@Override
	public boolean supportsResultSetConcurrency(int type, int concurrency) throws SQLException {
		return underlying.supportsResultSetConcurrency(type, concurrency);
	}

	@Override
	public boolean ownUpdatesAreVisible(int type) throws SQLException {
		return underlying.ownUpdatesAreVisible(type);
	}

	@Override
	public boolean ownDeletesAreVisible(int type) throws SQLException {
		return underlying.ownDeletesAreVisible(type);
	}

	@Override
	public boolean ownInsertsAreVisible(int type) throws SQLException {
		return underlying.ownInsertsAreVisible(type);
	}

	@Override
	public boolean othersUpdatesAreVisible(int type) throws SQLException {
		return underlying.othersUpdatesAreVisible(type);
	}

	@Override
	public boolean othersDeletesAreVisible(int type) throws SQLException {
		return underlying.othersDeletesAreVisible(type);
	}

	@Override
	public boolean othersInsertsAreVisible(int type) throws SQLException {
		return underlying.othersInsertsAreVisible(type);
	}

	@Override
	public boolean updatesAreDetected(int type) throws SQLException {
		return underlying.updatesAreDetected(type);
	}

	@Override
	public boolean deletesAreDetected(int type) throws SQLException {
		return underlying.deletesAreDetected(type);
	}

	@Override
	public boolean insertsAreDetected(int type) throws SQLException {
		return underlying.insertsAreDetected(type);
	}

	@Override
	public boolean supportsBatchUpdates() throws SQLException {
		return underlying.supportsBatchUpdates();
	}

	@Override
	public ResultSet getUDTs(String catalog, String schemaPattern, String typeNamePattern, int[] types) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getUDTs(catalog, schemaPattern, typeNamePattern, types), conn);
	}

	@Override
	public Connection getConnection() throws SQLException {
		return conn;
	}

	@Override
	public boolean supportsSavepoints() throws SQLException {
		return underlying.supportsSavepoints();
	}

	@Override
	public boolean supportsNamedParameters() throws SQLException {
		return underlying.supportsNamedParameters();
	}

	@Override
	public boolean supportsMultipleOpenResults() throws SQLException {
		return underlying.supportsMultipleOpenResults();
	}

	@Override
	public boolean supportsGetGeneratedKeys() throws SQLException {
		return underlying.supportsGetGeneratedKeys();
	}

	@Override
	public ResultSet getSuperTypes(String catalog, String schemaPattern, String typeNamePattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getSuperTypes(catalog, schemaPattern, typeNamePattern), conn);
	}

	@Override
	public ResultSet getSuperTables(String catalog, String schemaPattern, String tableNamePattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getSuperTables(catalog, schemaPattern, tableNamePattern), conn);
	}

	@Override
	public ResultSet getAttributes(String catalog, String schemaPattern, String typeNamePattern, String attributeNamePattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getAttributes(catalog, schemaPattern, typeNamePattern, attributeNamePattern), conn);
	}

	@Override
	public boolean supportsResultSetHoldability(int holdability) throws SQLException {
		return underlying.supportsResultSetHoldability(holdability);
	}

	@Override
	public int getResultSetHoldability() throws SQLException {
		return underlying.getResultSetHoldability();
	}

	@Override
	public int getDatabaseMajorVersion() throws SQLException {
		return underlying.getDatabaseMajorVersion();
	}

	@Override
	public int getDatabaseMinorVersion() throws SQLException {
		return underlying.getDatabaseMinorVersion();
	}

	@Override
	public int getJDBCMajorVersion() throws SQLException {
		return underlying.getJDBCMajorVersion();
	}

	@Override
	public int getJDBCMinorVersion() throws SQLException {
		return underlying.getJDBCMinorVersion();
	}

	@Override
	public int getSQLStateType() throws SQLException {
		return underlying.getSQLStateType();
	}

	@Override
	public boolean locatorsUpdateCopy() throws SQLException {
		return underlying.locatorsUpdateCopy();
	}

	@Override
	public boolean supportsStatementPooling() throws SQLException {
		return underlying.supportsStatementPooling();
	}

	@Override
	public RowIdLifetime getRowIdLifetime() throws SQLException {
		return underlying.getRowIdLifetime();
	}

	@Override
	public ResultSet getSchemas(String catalog, String schemaPattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getSchemas(catalog, schemaPattern), conn);
	}

	@Override
	public boolean supportsStoredFunctionsUsingCallSyntax() throws SQLException {
		return underlying.supportsStoredFunctionsUsingCallSyntax();
	}

	@Override
	public boolean autoCommitFailureClosesAllResultSets() throws SQLException {
		return underlying.autoCommitFailureClosesAllResultSets();
	}

	@Override
	public ResultSet getClientInfoProperties() throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getClientInfoProperties(), conn);
	}

	@Override
	public ResultSet getFunctions(String catalog, String schemaPattern, String functionNamePattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getFunctions(catalog, schemaPattern, functionNamePattern), conn);
	}

	@Override
	public ResultSet getFunctionColumns(String catalog, String schemaPattern, String functionNamePattern, String columnNamePattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getFunctionColumns(catalog, schemaPattern, functionNamePattern, columnNamePattern), conn);
	}

	@Override
	public ResultSet getPseudoColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern) throws SQLException {
		return WrapperUtils.getResultSet4ThinAir(underlying.getPseudoColumns(catalog, schemaPattern, tableNamePattern, columnNamePattern), conn);
	}

	@Override
	public boolean generatedKeyAlwaysReturned() throws SQLException {
		return underlying.generatedKeyAlwaysReturned();
	}
}
