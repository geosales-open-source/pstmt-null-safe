package br.com.softsite.pstmtnullsafe.jdbc;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.sql.Wrapper;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.function.Function;

import br.com.softsite.pstmtnullsafe.resource.Pool;
import br.com.softsite.pstmtnullsafe.resource.ReuseLastPool;
import br.com.softsite.pstmtnullsafe.resource.SingletonPool;

public class PstmtNullSafeConnection implements Connection, WrapperHelper {

	private final Connection underlying;
	private boolean optimizationForced = false;

	public PstmtNullSafeConnection(Connection underlying) {
		this.underlying = underlying;
	}

	@Override
	public Wrapper getUnderlying() {
		return underlying;
	}

	@Override
	public Statement createStatement() throws SQLException {
		return WrapperUtils.getStatement(underlying.createStatement(), this);
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return prepareStatementInternal(sql, getPoolCreator());
	}

	public PreparedStatement prepareStatementOpt(String sql) throws SQLException {
		return prepareStatementInternal(sql, SingletonPool::new);
	}

	public PreparedStatement prepareStatementCorrect(String sql) throws SQLException {
		return prepareStatementInternal(sql, ReuseLastPool::new);
	}

	private PreparedStatement prepareStatementInternal(String sql, Function<Function<NuloIds, PstmtImplNoNulls>, Pool<NuloIds, PstmtImplNoNulls>> poolCreator) throws SQLException {
		return new PstmtNullSafe(sql, underlying::prepareStatement, poolCreator, this);
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		throw new UnsupportedOperationException("Pstmt null safe driver does not support callabble statements");
	}

	@Override
	public String nativeSQL(String sql) throws SQLException {
		return underlying.nativeSQL(sql);
	}

	@Override
	public void setAutoCommit(boolean autoCommit) throws SQLException {
		underlying.setAutoCommit(autoCommit);
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
		return underlying.getAutoCommit();
	}

	@Override
	public void commit() throws SQLException {
		underlying.commit();
	}

	@Override
	public void rollback() throws SQLException {
		underlying.rollback();
	}

	@Override
	public void close() throws SQLException {
		underlying.close();
	}

	@Override
	public boolean isClosed() throws SQLException {
		return underlying.isClosed();
	}

	@Override
	public DatabaseMetaData getMetaData() throws SQLException {
		return new PstmtNullSafeDatabaseMetaData(underlying.getMetaData(), this);
	}

	@Override
	public void setReadOnly(boolean readOnly) throws SQLException {
		underlying.setReadOnly(readOnly);
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		return underlying.isReadOnly();
	}

	@Override
	public void setCatalog(String catalog) throws SQLException {
		underlying.setCatalog(catalog);
	}

	@Override
	public String getCatalog() throws SQLException {
		return underlying.getCatalog();
	}

	@Override
	public void setTransactionIsolation(int level) throws SQLException {
		underlying.setTransactionIsolation(level);
	}

	@Override
	public int getTransactionIsolation() throws SQLException {
		return underlying.getTransactionIsolation();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return underlying.getWarnings();
	}

	@Override
	public void clearWarnings() throws SQLException {
		underlying.clearWarnings();
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
		return WrapperUtils.getStatement(underlying.createStatement(resultSetType, resultSetConcurrency), this);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
		return prepareStatementInternal(sql, resultSetType, resultSetConcurrency, getPoolCreator());
	}

	public PreparedStatement prepareStatementOpt(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
		return prepareStatementInternal(sql, resultSetType, resultSetConcurrency, SingletonPool::new);
	}

	public PreparedStatement prepareStatementCorrect(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
		return prepareStatementInternal(sql, resultSetType, resultSetConcurrency, ReuseLastPool::new);
	}

	private PreparedStatement prepareStatementInternal(String sql, int resultSetType, int resultSetConcurrency, Function<Function<NuloIds, PstmtImplNoNulls>, Pool<NuloIds, PstmtImplNoNulls>> poolCreator) throws SQLException {
		return new PstmtNullSafe(sql, sqlNullSafe -> underlying.prepareStatement(sqlNullSafe, resultSetType, resultSetConcurrency), poolCreator, this);
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
		throw new UnsupportedOperationException("Pstmt null safe driver does not support callabble statements");
	}

	@Override
	public Map<String, Class<?>> getTypeMap() throws SQLException {
		return underlying.getTypeMap();
	}

	@Override
	public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
		underlying.setTypeMap(map);
	}

	@Override
	public void setHoldability(int holdability) throws SQLException {
		underlying.setHoldability(holdability);
	}

	@Override
	public int getHoldability() throws SQLException {
		return underlying.getHoldability();
	}

	@Override
	public Savepoint setSavepoint() throws SQLException {
		return underlying.setSavepoint();
	}

	@Override
	public Savepoint setSavepoint(String name) throws SQLException {
		return underlying.setSavepoint(name);
	}

	@Override
	public void rollback(Savepoint savepoint) throws SQLException {
		underlying.rollback(savepoint);
	}

	@Override
	public void releaseSavepoint(Savepoint savepoint) throws SQLException {
		underlying.releaseSavepoint(savepoint);
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		return WrapperUtils.getStatement(underlying.createStatement(resultSetType, resultSetConcurrency), this);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		return prepareStatementInternal(sql, resultSetType, resultSetConcurrency, resultSetHoldability, getPoolCreator());
	}

	public PreparedStatement prepareStatementOpt(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		return prepareStatementInternal(sql, resultSetType, resultSetConcurrency, resultSetHoldability, SingletonPool::new);
	}

	public PreparedStatement prepareStatementCorrect(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		return prepareStatementInternal(sql, resultSetType, resultSetConcurrency, resultSetHoldability, ReuseLastPool::new);
	}

	private PreparedStatement prepareStatementInternal(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability, Function<Function<NuloIds, PstmtImplNoNulls>, Pool<NuloIds, PstmtImplNoNulls>> poolCreator) throws SQLException {
		return new PstmtNullSafe(sql, sqlNullSafe -> underlying.prepareStatement(sqlNullSafe, resultSetType, resultSetConcurrency, resultSetHoldability), poolCreator, this);
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		throw new UnsupportedOperationException("Pstmt null safe driver does not support callabble statements");
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
		return prepareStatementInternal(sql, autoGeneratedKeys, getPoolCreator());
	}

	public PreparedStatement prepareStatementOpt(String sql, int autoGeneratedKeys) throws SQLException {
		return prepareStatementInternal(sql, autoGeneratedKeys, SingletonPool::new);
	}

	public PreparedStatement prepareStatementCorrect(String sql, int autoGeneratedKeys) throws SQLException {
		return prepareStatementInternal(sql, autoGeneratedKeys, ReuseLastPool::new);
	}

	private PreparedStatement prepareStatementInternal(String sql, int autoGeneratedKeys, Function<Function<NuloIds, PstmtImplNoNulls>, Pool<NuloIds, PstmtImplNoNulls>> poolCreator) throws SQLException {
		return new PstmtNullSafe(sql, sqlNullSafe -> underlying.prepareStatement(sqlNullSafe, autoGeneratedKeys), poolCreator, this);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
		return prepareStatementInternal(sql, columnIndexes, getPoolCreator());
	}

	public PreparedStatement prepareStatementOpt(String sql, int[] columnIndexes) throws SQLException {
		return prepareStatementInternal(sql, columnIndexes, SingletonPool::new);
	}

	public PreparedStatement prepareStatementCorrect(String sql, int[] columnIndexes) throws SQLException {
		return prepareStatementInternal(sql, columnIndexes, ReuseLastPool::new);
	}

	private PreparedStatement prepareStatementInternal(String sql, int[] columnIndexes, Function<Function<NuloIds, PstmtImplNoNulls>, Pool<NuloIds, PstmtImplNoNulls>> poolCreator) throws SQLException {
		return new PstmtNullSafe(sql, sqlNullSafe -> underlying.prepareStatement(sqlNullSafe, columnIndexes), poolCreator, this);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
		return prepareStatementInternal(sql, columnNames, getPoolCreator());
	}

	public PreparedStatement prepareStatementOpt(String sql, String[] columnNames) throws SQLException {
		return prepareStatementInternal(sql, columnNames, SingletonPool::new);
	}

	public PreparedStatement prepareStatementCorrect(String sql, String[] columnNames) throws SQLException {
		return prepareStatementInternal(sql, columnNames, ReuseLastPool::new);
	}

	private PreparedStatement prepareStatementInternal(String sql, String[] columnNames, Function<Function<NuloIds, PstmtImplNoNulls>, Pool<NuloIds, PstmtImplNoNulls>> poolCreator) throws SQLException {
		return new PstmtNullSafe(sql, sqlNullSafe -> underlying.prepareStatement(sqlNullSafe, columnNames), poolCreator, this);
	}

	@Override
	public Clob createClob() throws SQLException {
		return underlying.createClob();
	}

	@Override
	public Blob createBlob() throws SQLException {
		return underlying.createBlob();
	}

	@Override
	public NClob createNClob() throws SQLException {
		return underlying.createNClob();
	}

	@Override
	public SQLXML createSQLXML() throws SQLException {
		return underlying.createSQLXML();
	}

	@Override
	public boolean isValid(int timeout) throws SQLException {
		return underlying.isValid(timeout);
	}

	@Override
	public void setClientInfo(String name, String value) throws SQLClientInfoException {
		underlying.setClientInfo(name, value);
	}

	@Override
	public void setClientInfo(Properties properties) throws SQLClientInfoException {
		underlying.setClientInfo(properties);
	}

	@Override
	public String getClientInfo(String name) throws SQLException {
		return underlying.getClientInfo(name);
	}

	@Override
	public Properties getClientInfo() throws SQLException {
		return underlying.getClientInfo();
	}

	@Override
	public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
		return underlying.createArrayOf(typeName, elements);
	}

	@Override
	public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
		return underlying.createStruct(typeName, attributes);
	}

	@Override
	public void setSchema(String schema) throws SQLException {
		underlying.setSchema(schema);
	}

	@Override
	public String getSchema() throws SQLException {
		return underlying.getSchema();
	}

	@Override
	public void abort(Executor executor) throws SQLException {
		underlying.abort(executor);
	}

	@Override
	public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
		underlying.setNetworkTimeout(executor, milliseconds);
	}

	@Override
	public int getNetworkTimeout() throws SQLException {
		return underlying.getNetworkTimeout();
	}

	@Override
	public String toString() {
		return super.toString() + " over " + underlying;
	}

	public boolean isOptimizationForced() {
		return optimizationForced;
	}

	public void setOptimizationForced(boolean optimizationForced) {
		this.optimizationForced = optimizationForced;
	}

	public void forceOptimization() {
		this.optimizationForced = true;
	}

	public void forceCorrectness() {
		this.optimizationForced = false;
	}

	private Function<Function<NuloIds, PstmtImplNoNulls>, Pool<NuloIds, PstmtImplNoNulls>> getPoolCreator() {
		return optimizationForced? SingletonPool::new: ReuseLastPool::new;
	}
}
