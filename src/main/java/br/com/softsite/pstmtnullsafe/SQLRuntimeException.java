package br.com.softsite.pstmtnullsafe;

import java.sql.SQLException;

public class SQLRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 608792490104328519L;
	private final SQLException cause;

	public SQLRuntimeException(String message, SQLException cause) {
		super(message, cause);
		this.cause = cause;
	}

	public SQLRuntimeException(SQLException cause) {
		super(cause);
		this.cause = cause;
	}

	public SQLException getInnerException() {
		return cause;
	}
	
	@Override
	public synchronized SQLException getCause() {
		return cause;
	}
}