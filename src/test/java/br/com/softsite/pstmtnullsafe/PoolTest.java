package br.com.softsite.pstmtnullsafe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.com.softsite.pstmtnullsafe.resource.ReuseLastPool;
import br.com.softsite.pstmtnullsafe.resource.SingletonPool;
import br.com.softsite.toolbox.indirection.Indirection;

public class PoolTest {

	@Test
	public void singletonPoolTest() {
		SingletonPool<String, Indirection<String>> pool = new SingletonPool<>(Indirection::new);
		pool.putResource("marmota", new Indirection<>("M"));
		Indirection<String> r = pool.getResource("marmota");

		assertEquals("M", r.x);
		Indirection<String> r2 = pool.getResource("Marmota");
		Indirection<String> r3 = pool.getResource("Marmota");
		Indirection<String> r4 = pool.getResource("marmota");
		Indirection<String> r5 = pool.getResource("Marmota");

		assertEquals("Marmota", r2.x);
		assertEquals("Marmota", r3.x);
		assertEquals("M", r4.x);
		assertEquals("Marmota", r5.x);

		assertTrue(r == r4);
		assertTrue(r2 == r3);
		assertTrue(r2 == r5);

		assertEquals(2, pool.getAllocatedResources().size());

		pool.releaseAll();
		assertEquals(0, pool.getAllocatedResources().size());
	}

	@Test
	public void reusableLastPoolTest() {
		ReuseLastPool<String, Indirection<String>> pool = new ReuseLastPool<>(Indirection::new);
		Indirection<String> r = pool.getResource("marmota");

		assertEquals("marmota", r.x);
		Indirection<String> r2 = pool.getResource("Marmota");
		Indirection<String> r3 = pool.getResource("Marmota");
		Indirection<String> r4 = pool.getResource("marmota");
		Indirection<String> r5 = pool.getResource("Marmota");

		assertEquals("Marmota", r2.x);
		assertEquals("Marmota", r3.x);
		assertEquals("marmota", r4.x);
		assertEquals("Marmota", r5.x);

		assertTrue(r != r4);
		assertTrue(r2 == r3);
		assertTrue(r2 != r5);

		assertEquals(4, pool.getAllocatedResources().size());
		assertEquals(4, pool.getAllResources().size());

		pool.releaseAll();
		assertEquals(0, pool.getAllocatedResources().size());
		assertEquals(4, pool.getAllResources().size());

		Indirection<String> rr = pool.getResource("marmota");

		assertEquals("marmota", rr.x);
		Indirection<String> rr2 = pool.getResource("Marmota");
		Indirection<String> rr3 = pool.getResource("Marmota");
		Indirection<String> rr4 = pool.getResource("marmota");
		Indirection<String> rr5 = pool.getResource("Marmota");

		assertEquals("Marmota", rr2.x);
		assertEquals("Marmota", rr3.x);
		assertEquals("marmota", rr4.x);
		assertEquals("Marmota", rr5.x);

		assertTrue(r == rr);
		assertTrue(r2 == rr2);
		assertTrue(r3 == rr3);
		assertTrue(r4 == rr4);
		assertTrue(r5 == rr5);

		assertEquals(4, pool.getAllocatedResources().size());

		pool.releaseAll();
		assertEquals(4, pool.getAllResources().size());
	}
}
