package br.com.softsite.pstmtnullsafe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;

import org.junit.BeforeClass;
import org.junit.Test;

import br.com.softsite.pstmtnullsafe.jdbc.PstmtNullSafeDriver;
import br.com.softsite.toolbox.Pair;

public class DriverTest {

	@BeforeClass
	public static void init() {
		@SuppressWarnings("unused")
		Object o = PstmtNullSafeDriver.instance;
	}

	@Test
	public void getUrl() throws SQLException {
		assertTrue(PstmtNullSafeDriver.instance.acceptsURL("jdbc:pstmt-nullsafe:sqlite::memory:"));
		assertFalse(PstmtNullSafeDriver.instance.acceptsURL("jdbc:pstmt-nullsafe:sqllalo:lasjk"));
		assertFalse(PstmtNullSafeDriver.instance.acceptsURL("jdbc:pstmt-nufe:sqlite::memory:"));
	}

	@Test
	public void test1() throws SQLException {
		HashMap<Integer, String> resgatados = new HashMap<>();
		List<Pair<Integer, String>> pares = Arrays.asList(Pair.getPair(12, "abc"), Pair.getPair(13, "def"), Pair.getPair(0, null));

		try (Connection c = DriverManager.getConnection("jdbc:pstmt-nullsafe:sqlite::memory:");
				Statement stmt = c.createStatement()) {
			stmt.executeUpdate("CREATE TABLE A (id int, b text)");
			try (PreparedStatement pstmt = c.prepareStatement("INSERT INTO A (id, b) VALUES (?, ?)")) {
				for (Pair<Integer, String> par: pares) {
					pstmt.setInt(1, par.getKey());
					pstmt.setString(2, par.getValue());
					pstmt.addBatch();
				}
				int[] x = pstmt.executeBatch();
				assertEquals(pares.size(), x.length);
			}
			try (ResultSet rs = stmt.executeQuery("SELECT * FROM a")) {
				while (rs.next()) {
					resgatados.put(rs.getInt(1), rs.getString(2));
				}
			}
		}

		assertEquals(pares.size(), resgatados.size());
		Function<Integer, String> toStr = Pair.toInjectiveFunction(pares);
		for (Entry<Integer, String> es: resgatados.entrySet()) {
			assertEquals(toStr.apply(es.getKey()), es.getValue());
		}
	}

	@Test
	public void garantirInsercaoBatch() throws SQLException {
		HashMap<Integer, String> resgatados = new HashMap<>();
		List<Pair<Integer, String>> paresInsercao = Arrays.asList(Pair.getPair(12, "abc"), Pair.getPair(13, "def"), Pair.getPair(0, null));
		try (Connection c = DriverManager.getConnection("jdbc:pstmt-nullsafe:sqlite::memory:");
				Statement stmt = c.createStatement()) {
			stmt.executeUpdate("CREATE TABLE A (id int not null primary key, b text)");
			try (PreparedStatement pstmt = c.prepareStatement("INSERT INTO A (id, b) VALUES (?, ?)")) {
				for (Pair<Integer, String> par: paresInsercao) {
					pstmt.setInt(1, par.getKey());
					pstmt.setString(2, par.getValue());
					pstmt.addBatch();
				}
				int[] x = pstmt.executeBatch();
				assertEquals(paresInsercao.size(), x.length);
			}
			List<Pair<Integer, String>> paresTeste = Arrays.asList(Pair.getPair(-2, "abc"), Pair.getPair(13, null), Pair.getPair(-1, "lalala"));
			try (PreparedStatement pstmt = c.prepareStatement("INSERT INTO A (id, b) SELECT ? AS ID, ? AS B WHERE NOT EXISTS (SELECT 1 FROM A WHERE A.id = ? LIMIT 1)")) {
				for (Pair<Integer, String> par: paresTeste) {
					int idTeste = par.getKey();
					String valueTeste = par.getValue();

					pstmt.setInt(1, idTeste);
					pstmt.setString(2, valueTeste);
					pstmt.setInt(3, idTeste);
					pstmt.addBatch();
				}
				int[] x = pstmt.executeBatch();
				assertEquals(paresTeste.size(), x.length);
				assertEquals(1, x[0]);
				assertEquals(0, x[1]);
				assertEquals(1, x[2]);
			}
			try (PreparedStatement pstmt = c.prepareStatement("SELECT * FROM a");
					ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					resgatados.put(rs.getInt(1), rs.getString(2));
				}
			}
		}
		List<Pair<Integer, String>> paresResultados = Arrays.asList
				(
						Pair.getPair(12, "abc"),
						Pair.getPair(13, "def"),
						Pair.getPair(0, null),
						Pair.getPair(-2, "abc"),
						Pair.getPair(-1, "lalala")
				);
		assertEquals(paresResultados.size(), resgatados.size());
		Function<Integer, String> toStr = Pair.toInjectiveFunction(paresResultados);
		for (Entry<Integer, String> es: resgatados.entrySet()) {
			assertEquals(toStr.apply(es.getKey()), es.getValue());
		}
	}
}
